module.exports = {
  pages: {
    'index': {
      entry: './src/pages/Home/main.js',
      template: 'public/index.html',
      title: 'Home',
      chunks: [ 'chunk-vendors', 'chunk-common', 'index' ]
    },
    'addevent': {
      entry: './src/pages/Addevent/main.js',
      template: 'public/index.html',
      title: 'Addevent',
      chunks: [ 'chunk-vendors', 'chunk-common', 'addevent' ]
    },
    'eventdetails': {
      entry: './src/pages/Eventdetails/main.js',
      template: 'public/index.html',
      title: 'Addevent',
      chunks: [ 'chunk-vendors', 'chunk-common', 'eventdetails' ]
    }
  }
}
