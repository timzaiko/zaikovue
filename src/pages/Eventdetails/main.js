import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './../../plugins/bootstrap-vue'
import BootstrapVue from 'bootstrap-vue'

// font awesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import App from './EventDetails.vue'

library.add(fas, fab, far)
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(BootstrapVue)

import './../../scss/app.css'

new Vue({
  render: h => h(App),
}).$mount('#guestadmin')
